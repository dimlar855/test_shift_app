from models import user_model
from sqlalchemy.orm import Session
from schemas import user_schema


def add_user(data: user_schema.User, db: Session):
    user = user_model.User(fio=data.fio,
                           login=data.login,
                           hash_password=data.password)
    try:
        db.add(user)
        db.commit()
        db.refresh(user)
    except Exception as e:
        print(e)
    return user



