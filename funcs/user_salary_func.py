import datetime
from models.user_salary_model import UserSalary
from sqlalchemy.orm import Session
from schemas import user_salary_schema


def add_user_salary(data: user_salary_schema.UserSalary, db: Session):
    user_salary = UserSalary(user_id=data.user_id,
                             salary=data.salary,
                             promotion_date=data.promotion_date)
    try:
        db.add(user_salary)
        db.commit()
        db.refresh(user_salary)
    except Exception as e:
        print(e)
    return user_salary


def get_user_info(user_id: int, db: Session):
    salary = db.query(UserSalary.salary).filter(
                                            UserSalary.user_id == user_id,
                                            UserSalary.promotion_date <= datetime.date.today()
                                            ).order_by(UserSalary.promotion_date.desc()).first()
    salary = str(*salary) if salary else "нет данных"
    date = db.query(UserSalary.promotion_date).filter(
                                                UserSalary.user_id == user_id,
                                                UserSalary.promotion_date > datetime.date.today()
                                                ).order_by(UserSalary.promotion_date).first()
    date = str(*date) if date else "нет данных"
    return {"current_salary": salary, "next_promotion_date": date}
