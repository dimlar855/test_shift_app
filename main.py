import uvicorn
from fastapi import FastAPI
from database import engine, Base
from routers import user_router, user_salary_router

Base.metadata.create_all(bind=engine)
app = FastAPI()
app.include_router(user_router.router, prefix="/user")
app.include_router(user_salary_router.router, prefix="/user_info")

if __name__ == "__main__":
    uvicorn.run("main:app", host="localhost", port=8080, reload=True, workers=3)
