from sqlalchemy import Column, Integer, String
from database import Base


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    fio = Column(String, nullable=False)
    login = Column(String, unique=True, nullable=False)
    role = Column(String, default='user', nullable=False)
    hash_password = Column(String, nullable=False)
