from sqlalchemy import Column, Integer, String, Float, ForeignKey
from database import Base


class UserSalary(Base):
    __tablename__ = 'user_salary'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("user.id"))
    salary = Column(Float(2), nullable=False)
    promotion_date = Column(String, nullable=False)
