from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from database import get_db
from schemas import user_schema
from funcs import user_func

router = APIRouter()


@router.post("/")
async def add(data: user_schema.User = None, db: Session = Depends(get_db)):
    return {"status": 200, "data": user_func.add_user(data, db)}
