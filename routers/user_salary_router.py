from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from database import get_db
from schemas import user_salary_schema
from funcs import user_salary_func

router = APIRouter()


@router.post("/")
async def add(data: user_salary_schema.UserSalary = None, db: Session = Depends(get_db)):
    return {"status": 200, "data": user_salary_func.add_user_salary(data, db)}


@router.get("/{user_id}")
async def get(user_id: int = None, db: Session = Depends(get_db)):
    return {"status": 200, "data": user_salary_func.get_user_info(user_id, db)}
