from typing import Optional
from pydantic import BaseModel


class UserSalary(BaseModel):
    user_id: Optional[int]
    salary: Optional[float] = "нет данных"
    promotion_date: Optional[str] = "нет данных"
