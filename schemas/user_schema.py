from pydantic import BaseModel


class User(BaseModel):
    fio: str
    login: str
    password: str
